package io.backend.aios.error

class OrderException(message: String): Exception(message)