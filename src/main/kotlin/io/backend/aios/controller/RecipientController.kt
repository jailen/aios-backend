package io.backend.aios.controller

import io.backend.aios.database.Database
import io.backend.aios.model.Recipient
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@RestController
class RecipientController {

    @Autowired
    private lateinit var database: Database

    @RequestMapping("/recipients", method = arrayOf(RequestMethod.GET))
    fun recipients() = database.recipients

    @PostMapping("/recipients/add")
    fun newRecipient(@RequestBody recipient: Recipient?) {
        recipient?.apply {
            if (!checkRecipientExistence(this)) {
                database.recipients.add(this)
            }
        }
    }

    // MARK: Checks.

    // Determines if the Recipient is already contained in the database.
    fun checkRecipientExistence(recipient: Recipient): Boolean {
        return database.recipients.firstOrNull { it.name == recipient.name } != null
    }
}