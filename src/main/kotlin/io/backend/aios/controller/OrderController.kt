package io.backend.aios.controller

import io.backend.aios.database.Database
import io.backend.aios.model.order.Order
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*


@RestController
class OrderController {

    @Autowired
    private lateinit var database: Database

    @RequestMapping("/orders", method = arrayOf(RequestMethod.GET))
    fun orders() = database.orders


    @GetMapping("/orders/{recipient_id}")
    fun recipientOrdersByID( @PathVariable("recipient_id") recipient_id: String) : List<Order> {
        database.findRecipientBy(UUID.fromString(recipient_id))?.apply {
            return database.ordersBy(this)
        }
        return listOf()
    }
}