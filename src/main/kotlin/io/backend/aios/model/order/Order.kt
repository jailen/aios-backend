package io.backend.aios.model.order

import io.backend.aios.error.OrderException
import io.backend.aios.model.Identificable
import io.backend.aios.model.Recipient
import io.backend.aios.model.guard
import java.time.Duration
import java.time.LocalDateTime
import java.util.*


data class Order(
    // Identifier of the order.
        override val identifier: UUID = UUID.randomUUID(),
    // Destination of the order.
        var recipient: Recipient,
    // Intended delivery date of the order.
        var date: LocalDateTime,
    // Amount of bananas, measured in kilograms.
        var amount: Float,
    // Cost of the order.
        var price: Float
) : Identificable {

    init {
        // Checks that the provided order date follows minimum rule date specification.
        checkNeWOrderDate(date).guard { throw(OrderException("The date provided is not correct.")) }
        // Checks the recipient itself: if the name is not already in the database,
        // then it adds it.
        //checkRecipient(recipient)
    }

    // MARK - Checks.

    // Ensures that the order date has is at least a week (seven days) forward
    // in time with the given datetime object.
    fun checkNeWOrderDate(date: LocalDateTime): Boolean {
        val now = LocalDateTime.now()
        val duration = Duration.between(now, date)
        return duration.toDays() > 7
    }
}