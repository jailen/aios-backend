package io.backend.aios.model

import java.util.*


/*
    Recipient holds information about the final beneficiary
    or devisee of an Order.
 */
data class Recipient(
    override val identifier: UUID = UUID.randomUUID(),
    var name: String,
    var address: String,
    var postalCode: String,
    var city: String,
    var country: String
) : Identificable
