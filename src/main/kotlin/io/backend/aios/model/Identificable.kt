package io.backend.aios.model

import java.util.*

interface Identificable {

    val identifier: UUID
}