package io.backend.aios.model

// Calls a guard lambda block if the value is null.
inline fun <T> T.guard(block: T.() -> Unit): T {
    if (this == null) block(); return this
}

// Replaces an item within an Iterable given an index.
fun <E> Iterable<E>.updated(index: Int, elem: E) = mapIndexed {
        i, existing ->  if (i == index) elem else existing
}

// Replaces a Recipient item within a new one given its identifier.
fun <I: Identificable> Iterable<I>.updateWith(new: I) = map {
        item -> if (item.identifier == new.identifier) new else item }
