package io.backend.aios.database

import io.backend.aios.model.order.Order
import io.backend.aios.model.Recipient
import org.springframework.stereotype.Component
import java.time.LocalDateTime
import java.util.*

@Component
final class Database(
        // All orders managed by the item.
        var orders: MutableList<Order> = mutableListOf(),

        // Lists all managed recipient.
        var recipients: MutableList<Recipient> = mutableListOf()
    ) {
    // Initialises the database, with some mockup data.
    init {
        recipients.add(Recipient(name = "John Connor",
                address = "505 Covet Street, 2B", postalCode = "10001F", city = "Los Angeles", country = "USA"))
        recipients.add(Recipient(name = "Tom Marvolo Riddle",
                address = "The Old Shack House", postalCode = "666A", city = "Hogsmeade", country = "UK"))
        // These mockup should display if the app is able to retrieve successfully already present Recipients.
        findRecipientBy("John Connor")?.apply {
            orders.add(Order(
                    recipient = this,
                    date = LocalDateTime.now().plusDays(10),
                    amount = 20.0F,
                    price = 100.0F
            ))
            orders.add(Order(
                    recipient = this,
                    date = LocalDateTime.now().plusDays(2334),
                    amount = 545.0F,
                    price = 5653.0F
            ))
        }
        findRecipientBy("Tom Marvolo Riddle")?.apply {
            orders.add(Order(
                    recipient = this,
                    date = LocalDateTime.now().plusDays(10),
                    amount = 20.0F,
                    price = 100.0F
            ))
            orders.add(Order(
                    recipient = this,
                    date = LocalDateTime.now().plusDays(2334),
                    amount = 545.0F,
                    price = 5653.0F
            ))
        }
    }

    // MARK: Functions.

    // Provides all orders from a given recipient.
    fun ordersBy(recipient: Recipient): List<Order> {
        return orders.filter { it.recipient == recipient }
    }

    fun findOrderBy(identifier: UUID): Order? {
        return orders.firstOrNull { it.identifier == identifier }
    }

    fun findRecipientBy(identifier: UUID): Recipient? {
        return recipients.firstOrNull { it.identifier == identifier }
    }

    fun findRecipientBy(name: String): Recipient? {
        return recipients.firstOrNull { it.name == name }
    }
}